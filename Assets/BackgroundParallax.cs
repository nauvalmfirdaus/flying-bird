﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundParallax : MonoBehaviour
{
    //Objek Background
    public Transform[] backgrounds;

    //Referensi objek obstacle yang akan di generate
    public Transform spawnPosition;

    //Nilai kecepatan pergerakan background
    public Vector3 moveSpeed;

    //Variable untuk menilai posisi bacakground local terhadap layar
    Vector3 screenPosition;

    //Nilai apakah background berpempat pada layar
    bool onScreen;

    //Method akan dipanggil setelah method Start() dan akan diulang setiap frame per second (FPS)
    private void Update()
    {
        foreach (Transform background in backgrounds)
        {
            /*Mengungari posisi background dengan nilai moveSpeed setiap satuan waktu pada game
             * agar bergerak ke kiri*/
            background.position -= moveSpeed * Time.deltaTime;

            //Mengambil nilai posisi background local terhadap layar
            screenPosition = Camera.main.ScreenToViewportPoint(background.position);

            //Menempatkan background ke posisi spawnPosition jika keluar dari batas layar
            onScreen = screenPosition.x > 0;
            if (!onScreen)
                background.position = spawnPosition.position;
        }
    }
}
