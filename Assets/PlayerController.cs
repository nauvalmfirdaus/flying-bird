﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; //namespace untuk mengakses namespace SceneManagement

public class PlayerController : MonoBehaviour {

    //Variable untuk seberapa besar gaya dorong lompat player
    public Vector2 jumpForce = new Vector2(0, 10000);

    //Variable untuk menilai jeda perputaran player oleh gravitasi
    public float tiltDrag = 8f;

    //Variable pengakses komponen Rigidbody2D
    Rigidbody2D rb;

    //Variable penyedia rotasi dengan jeda perputaran player oleh gravitasi
    Quaternion tiltRotation;

    //Variable untuk menilai posisi obstacle local terhadap layar
    Vector2 screenPosition;

    //Method akan dipanggil saat Scene dimuat
    private void Start()
    {
        //Mengambil referensi komponen Rigidbody2D player
        rb = GetComponent<Rigidbody2D>();
    }

    //Method akan dipanggil setelah method Start() dan akan diulang setiap frame per second (FPS)
    private void Update()
    {
        //Membaca Inputan klik kiri pada mouse
        if (Input.GetMouseButtonDown(0))
        {
            //Men-set kecepatan (velocity) Rigidbody2D player menjadi 0
            rb.velocity = Vector2.zero;

            /*Menambahkan gaya pada Rigidbody2D dengan nilai dari variable jumpForce, 
             * sehingga player akan melakukan lompatan keatas*/
            rb.AddForce(jumpForce);

            //Mengambil referensi komponen AudioSource kemudian memainkan musiknya
            GetComponent<AudioSource>().Play();
        }

        /*Men-set variable tiltRotation ke perhitungan untuk mendapatkan nilai rotasi yang 
         * sesuai dengan nilai kecepatan (velocity) Rigidbody2D player*/
        tiltRotation = Quaternion.Euler(new Vector3(0f, 0f, rb.velocity.y / tiltDrag));

        //Men-set rotasi player sesuai dengan nilai variable tiltRotation
        transform.rotation = tiltRotation;

        //Mengambil nilai posisi player local terhadap layar
        screenPosition = Camera.main.ScreenToViewportPoint(transform.position);

        //Jika posisi player melewati batas bawah atau atas layar, maka matikan player
        if (screenPosition.y > 1 || screenPosition.y < 0)
            Die();
    }

    //Method dipanggil untuk mematikan player
    void Die()
    {
        //Jika player mati, maka mengulangi Scene, dan game dimulai dari awal
        SceneManager.LoadScene(0);
    }

    //Jika player menabrak obstacle maka matikan player
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Die();
    }
}
