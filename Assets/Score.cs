﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    //Variable penampung nilai score
    public int score;

    //Variable UI untuk men-display nilai score
    public Text scoreText;

    //Referensi tunggal sampel dari class Score
    static Score instance;
    public static Score Instance
    {
        get
        {
            //Jika sampel belum ada, maka refernsi sampel dicari lewat pencarian objek dengan class Score
            if (!instance)
                instance = FindObjectOfType<Score>();

            //Mengembalikan refernsi tunggal sampel dari class Score
            return instance;
        }
    }

    //Method akan dipanggil saat Scene dimuat
    private void Start()
    {
        //Set score menjadi 0 ketika permainan dimulai
        score = 0;

        //Update UI Score
        UpdateScore();
    }

    //Method untuk menambahkan score dengan nilai 1
    public void AddScore()
    {
        //Menambah nilai score dengan 1
        score += 1;

        //Update UI Score
        UpdateScore();
    }

    //Method untuk men-display nilai score pad UI
    void UpdateScore()
    {
        //Set UI untuk men-display nilai score dengan nilai variable score 
        scoreText.text = "Score : " + score;
    }
}
