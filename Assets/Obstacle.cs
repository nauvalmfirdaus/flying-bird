﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    //Variable untuk meniali kecepatan obstacle bergerak
    public Vector2 moveSpeed = new Vector2(-40, 0);

    //Variable untuk menilai posisi obstacle local terhadap layar
    Vector2 screenPosition;

    //Variable nilai deteksi dari Raycast2D
    RaycastHit2D hit;

    //Variable nilai apakah ray terkena objek apa tidak
    bool rayHitted;

    //Variable apakah obsatcle ini terbalik apa tidak
    public bool isInvert = false;

    //Method akan dipanggil setelah method Start() dan akan diulang setiap frame per second (FPS)
    private void Update()
    {
        //Obstacle akan bergerak ke kiri dengan kecepatan sesuai variable moveSpeed setiap satuan waktu pada game
        transform.Translate(moveSpeed * Time.deltaTime, Space.World);

        //Mengambil nilai posisi obstacle local terhadap layar
        screenPosition = Camera.main.ScreenToViewportPoint(transform.position);

        //Menon-aktifkan obstacle jika keluar dari batas layar
        if (screenPosition.x < 0)
            Terminate();
    }

    //Method untuk menon-aktifkan obstacle
    void Terminate()
    {
        gameObject.SetActive(false);
    }

    //Method akan dipanggil setelah method Start() dan akan diulang setiap satuan waktu Fixed Update Time
    private void FixedUpdate()
    {
        //Jika ray belu mengenai objek, maka tetap lakukan pendeteksian
        if (!rayHitted)
        {
            //Mengambil nilai deteksi dari Raycast2D
            hit = Physics2D.Raycast(transform.position, Vector2.up * (isInvert? -1 : 1) , Mathf.Infinity, 1 << 8);
           
            //Jika variable hit tidak kosong dan memiliki komponen PlayerController, maka lakukan penambahan score
            if (hit && hit.collider.GetComponent<PlayerController>())
            {
                //Mengambil referensi komponen AudioSource kemudian memainkan musiknya
                GetComponent<AudioSource>().Play();

                //Eksekusi method AddScore pada class Score
                Score.Instance.AddScore();

                //Set ray bahwa telah mengenai objek
                rayHitted = true;
            }
        }
    }

    //Method akan dipanggil ketika objek obstacle diaktifkan
    private void OnEnable()
    {
        //Set ray bahwa belum mengenai sesuatu
        rayHitted = false;
    }
}
