﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleGenerator : MonoBehaviour
{
    //Referensi posisi tempat obstacle akan di tempatkan
    public Transform spawnPosition;

    //Referensi objek obstacle yang akan di generate
    public GameObject obstacle;

    //Waktu jeda sebelum obstacle munculkan
    public float startTime = 1f;

    //Jeda waktu pengulangan pemunculan obstacle
    public float repeatTime = 8f;

    //Offset posisi sebagai nilai posisi vertical alternatif obstacle
    public float partyPositionOffset = 50;

    //List obstacle yang telah di munculkan
    List<GameObject> obstacleList = new List<GameObject>();

    //Method akan dipanggil saat Scene dimuat
    private void Start()
    {
        //Membuat obstacle setelah waktu nilai startTime dan mengulangi dengan jeda waktu repeatTime
        InvokeRepeating("CreateObstacle", startTime, repeatTime);
    }

    //Method pembuat obstacle
    void CreateObstacle()
    {
        //Index random antara angka 0 dan 1
        int randomIndex = Random.Range(0, 2);

        //Jika di List obstacleList ada objeknya, maka tinggal menggunakan objek yang non-aktif kembali aktif
        if (obstacleList.Count > 0)
        {
            foreach (GameObject obstacle in obstacleList)
            {
                //Jika ada objek obstacle yang non-aktif, maka dapat digunakan kembali
                if (!obstacle.activeInHierarchy)
                {
                    //Men-set obstacle yang non-aktif untuk aktif kembali dan ditempatkan pada posisi spawnPosition
                    obstacle.SetActive(true);
                    obstacle.transform.position = spawnPosition.position;

                    //Jika random Index = 1, maka obstacle dioffset senilai partyPositionOffset
                    if (randomIndex == 1)
                        obstacle.transform.position -= new Vector3(0f, partyPositionOffset, 0f);

                    return;
                }
            }
        }

        //Memunculkan objek obstacle pada posisi spawnPosition
        GameObject temp = Instantiate(obstacle, transform);
        temp.transform.position = spawnPosition.position;
        obstacleList.Add(temp);

        //Jika random Index = 1, maka obstacle dioffset senilai partyPositionOffset
        if (randomIndex == 1)
            temp.transform.position -= new Vector3(0f, partyPositionOffset, 0f);
    }
}
